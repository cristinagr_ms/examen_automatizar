describe('Test Loguin', () => {
  it('Loguin exitoso', () => {
    cy.visit('https://www.automationexercise.com/')
    cy.get('.shop-menu > .nav > :nth-child(4) > a').click()
    cy.get('[data-qa="login-email"]').type('giuliana@gmail.com')
    cy.get('[data-qa="login-password"]').type('1234')
    cy.get('[data-qa="login-button"]').click()
   
  });

  it('Loguin incorrecto', () => {
    cy.visit('https://www.automationexercise.com/')
    cy.get('.shop-menu > .nav > :nth-child(4) > a').click()

    cy.get('[data-qa="login-email"]').type('giul@gmail.com')
    cy.get('[data-qa="login-password"]').type('123456')

    cy.get('[data-qa="login-button"]').click()
    cy.get('.login-form > form > p').should('contain.text','Your email or password is incorrect!')

  });
  });